# Maîtrise de poste - Day 1

L'objectif de ce TP est de mieux vous faire appréhender l'OS dans lequel vous évoluer tous les jours.

**TOUT** doit être fait depuis la ligne de commande sauf contre-indication. 

**TOUT** doit être réalisé sur votre PC, sur votre OS (pas de VMs, sauf contre-indication).

On va explorer :
* les informations liées à votre OS (liste de périphériques, utilisateurs, etc.)
* l'utilisation des périphériques (partitions de disque, port réseau, etc.)
* la sécurité (sujets autour du chiffrement, la signature, etc.)
* le téléchargement de logiciels (gestionnaire de paquets)
* le contrôle à distance (SSH)
* exposition de service réseau local
* la notion de certificats et de confiance (TLS, chiffrement de mails)

## Index 

<!-- vim-markdown-toc GitLab -->

* [I. Self-footprinting](#i-self-footprinting)
    * [1. Host OS](#1-host-os)
    * [2. Devices](#2-devices)
    * [3. Users](#3-users)
    * [4. Processus](#4-processus)
    * [5. Network](#5-network)
* [II. Scripting](#ii-scripting)
* [III. Gestion de softs](#iii-gestion-de-softs)
* [IV. Partage de fichiers](#iv-partage-de-fichiers)

<!-- vim-markdown-toc -->

# I. Self-footprinting

Première étape : prendre connaissance du système. Etapes peut-être triviales mais néanmoins nécessaires.

**Détailler la marche à suivre pour chacune des étapes.**

## 1. Host OS

🌞 Déterminer les principales informations de votre machine 
* nom de la machine
* OS et version
* architecture processeur (32-bit, 64-bit, ARM, etc)
* quantité RAM et modèle de la RAM

## 2. Devices

Travail sur les périphériques branchés à la machine.

🌞 Trouver
* la marque et le modèle de votre processeur
  * identifier le nombre de processeurs, le nombre de coeur
  * si c'est un proc Intel, expliquer le nom du processeur
* la marque et le modèle :
  * de votre touchpad/trackpad
  * de votre carte graphique

🌞 Disque dur
* identifier la marque et le modèle de votre(vos) disque(s) dur(s)
* identifier les différentes partitions de votre/vos disque(s) dur(s)
* déterminer le système de fichier de chaque partition
* expliquer la fonction de chaque partition

## 3. Users                                                                            

🌞 Déterminer la liste des utilisateurs de la machine
* la liste **complète** des utilisateurs de la machine (je vous vois les Windowsiens...)
* déterminer le nom de l'utilisateur qui est full admin sur la machine
  * il existe toujours un utilisateur particulier qui a le droit de tout faire sur la machine
  * pour les Windowsiens : faites des recherches sur `NT-AUTHORITY\SYSTEM` et le concept de SID

## 4. Processus

🌞 Déterminer la liste des processus de la machine
* je vous épargne l'explication de chacune des lignes
  * bien que ça soit plutôt cool de connaître tous les programmes qui tournent sur sa machine
  * ça peut vite devenir indispensable sur un serveur que l'on veut optimiser
* choisissez 5 services système et expliquer leur utilité
  * par "service système" j'entends des processus élémentaires au bon fonctionnement de la machine
  * sans eux, l'OS tel qu'on l'utilise n'existe pas
  * exemple : interface graphique, démon réseau, etc.
* déterminer les processus lancés par l'utilisateur qui est full admin sur la machine

## 5. Network

🌞 Afficher la liste des cartes réseau de votre machine
* expliquer la fonction de chacune d'entre elles
  * exemple : carte WiFi, carte de loopback, etc.

🌞 Lister tous les ports TCP et UDP en utilisation
* déterminer quel programme tourne derrière chacun des ports
* expliquer la fonction de chacun de ces programmes

# II. Scripting

Le scripting est une approche du développement qui consiste à automatiser de petites tâches simples, mais réalisées à intervalles réguliers ou un grand nombre de fois. 

L'objectif de cette partie est de manipuler un langage de script natif à votre OS. Les principaux avantages d'utiliser un langage natif :
* bah... c'est natif ! Pas besoin d'installation, et il est intégré au système
* le langage est mis à jour automatiquement, en même temps que le système
* le langage est rapide, car souvent bien intégré à l'environnement
* il permet d'accéder de façon simples aux ressources de l'OS (périphériques, processus, etc.) et de les manipuler de façon tout aussi aisée

Vous devrez :
* commenter votre script
* mettre un en-tête à votre script
  * l'en-tête c'est juste un ensemble de commentaires qui renseignent sur le script
  * doivent figurer dans l'en-tête :
    * shebang si besoin (GNU/Linux et MacOS)
    * auteur
    * date
    * description **succincte** de ce que fait le script

🌞 Utiliser un langage de scripting natif à votre OS
* trouvez un langage natif par rapport à votre OS
* l'utiliser pour coder un script qui
  * affiche un résumé de l'OS
    * nom machine
    * IP principale
    * OS et version de l'OS
    * date et heure d'allumage
    * détermine si l'OS est à jour
    * Espace RAM utilisé / Espace RAM dispo
    * Espace disque utilisé / Espace disque dispo
  * liste les utilisateurs de la machine
  * calcule et affiche le temps de réponse moyen vers `8.8.8.8`
    * avec des `ping`
    * **NB :** si vous êtes dans les locaux d'YNOV, le ping vers `8.8.8.8` est impossible. Vous pouvez remplacer par `10.33.3.253` (cette IP n'est joignable **QUE** depuis le réseau d'YNOV)

🐙 ajouter des fonctionnalités au script
* calcule et affiche le débit maximum en download et upload vers internet

Exemple d'output : 
```bash
$ ./footprinting.sh 
Get average ping latency to 8.8.8.8 server...
Get average upload speed...
Get average download speed...

----------------------------------------
nowhere.localhost footprinting report :
----------------------------------------
OS : Arch Linux
Linux kernel : 5.5.9-arch1-2
Is OS up-to-date : false
Up since : 2020-04-29 09:03:07

Interface wlp4s0 : 192.168.1.57/24
  - 8.8.8.8 average ping time : 15.623 ms
  - average download speed : 349.59 Mbit/s
  - average upload speed : 217.76 Mbit/s

RAM
  Used : 3.4Gi
  Free : 2.0Gi
Disk
  Used : 130G
  Free : 90G

Users list : root bin daemon mail ftp http nobody dbus systemd-journal-remote systemd-network systemd-resolve systemd-timesync systemd-coredump uuidd it4 avahi colord polkitd rtkit lightdm usbmux git cups nx nm-openvpn docker gluster rpc netdata dnsmasq toto redis test-cesi tor
```


---

🌞 Créer un deuxième script qui permet, en fonction d'arguments qui lui sont passés :
* exécuter une action
  * lock l'écran après X secondes
  * éteindre le PC après X secondes
* exemple d'utilisation :

```bash
# Verrouile le PC après 30 secondes
$ ./tp1_script2.sh lock 30

# Eteins le PC après 75 secondes
$ ./tp1_script2.sh shutdown 75
```

# III. Gestion de softs

Tous les OS modernes sont équipés ou peuvent être équipés d'un gestionnaire de paquets. Par exemple :
* `apt` pour les GNU/Linux issus de Debian
* `dnf` pour les GNU/Linux issus de RedHat
* `brew` pour macOS
* `chocolatey` pour Windows

🌞 Expliquer l'intérêt de l'utilisation d'un gestionnaire de paquets
* par rapport au téléchargement en direct sur internet
* penser à l'identité des gens impliqués dans un téléchargement (vous, l'éditeur logiciel, etc.)
* penser à la sécurité globale impliquée lors d'un téléchargement

🌞 Utiliser un gestionnaire de paquet propres à votre OS pour
* lister tous les paquets déjà installés
* déterminer la provenance des paquets (= quel serveur nous délivre les paquets lorsqu'on installe quelque chose)

# IV. Partage de fichiers

Un serveur de fichiers permet de partager des fichiers sur le réseau. Il est bon de maîtriser un outil natif (ou très proche) de l'OS afin de lancer un partage ou y accéder. 

Les solutions les plus répandues :
* Windows : partage Samba
* GNU/Linux ou MacOS : NFS ou Samba

> Sur Windows ça peut se faire avec un simple 'Clic droit > Propriétés > Partager' ou quelque chose comme ça.

🌞 Monter un partage de fichiers
* prouver que le service est actif
  * prouver qu'un processus/service est dédié à ce partage
  * prouver qu'un port réseau permet d'y accéder
* prouver qu'un client peut y accéder
